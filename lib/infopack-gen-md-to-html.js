var Promise = require('bluebird');
var fs = require('fs');
var fg = require('fast-glob');
var marked = require('marked');

marked.marked.use({ extensions: [require('./marked-ext-aoatotable')] })

module.exports.step = function step(settings) {
    return {
        run: function(executor) {
            return fg('input/**/*.md')
                .then(paths => {
                    return Promise.mapSeries(paths, filePath => {
                        let newFilePath = filePath.replace('input/', '')
                        var meta = JSON.parse(fs.readFileSync(executor.getInputPath(newFilePath + '.meta-input.json')))
                        var fileOutputPath = newFilePath.replace('.md', '.partial.html')
                        var md = fs.readFileSync(filePath)
                        executor.toOutput({
                            path: fileOutputPath,
                            data: marked.marked.parse(md.toString(), { currentFilePath: filePath, executor: executor }),
                            title: meta.title,
                            description: meta.description,
                            labels: meta.labels,
                            origin: [newFilePath]
                        })

                    })   
                })
        },
        settings: settings || {}
    }
}
