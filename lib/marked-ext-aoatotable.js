var path = require('path')
var fs = require('fs')
var escapeString = require('escape-html')

function aoaToHtml(aoa, footnote) {
    let output = '<table class="marked-table">\n  <tbody>\n'
    aoa.forEach((row) => {
        output += '    <tr>\n'
        row.forEach((cell) => {
            output += `      <td>${escapeString(cell)}</td>\n`
        })
        output += '    </tr>\n'
    })
    return output += `  </tbody>\n</table>\n<p>${footnote}</p>\n`
}

module.exports = {
    name: 'aoaToTable',
    level: 'block',                                 // Is this a block-level or inline-level tokenizer?
    start(src) { return src.match(/aoaToTable\[/)?.index; },    // Hint to Marked.js to stop and check for a match
    tokenizer(src, tokens) {
      const rule = /^aoaToTable\[(.*)\]\((.*)\)/;  // Regex for the complete token, anchor to string start
      const match = rule.exec(src);
      if (match) {
        return {
          type: 'aoaToTable',
          raw: match[0],
          description: this.lexer.inlineTokens(match[1].trim()),
          relativePath: match[2].trim()
        };
      }
    },
    renderer(token) {
        var aoaPath = this.parser.options.executor.getInputPath(path.join(path.dirname(this.parser.options.currentFilePath.replace('input/', '')), token.relativePath))
        try {
            var aoa = JSON.parse(fs.readFileSync(aoaPath))
        } catch (error) {
            if(error.code == 'ENOENT') {
                console.error('Could not resolve json file at path: ', aoaPath, 'referenced in file: ' + this.parser.options.currentFilePath);
                exit(-1);
            }
            if(error.name == 'SyntaxError') {
                console.error('Could not parse json file at path: ', aoaPath, 'referenced in file: ' + this.parser.options.currentFilePath);
                exit(-1);
            }
            throw error
        }
        return aoaToHtml(aoa, this.parser.parseInline(token.description))
    }
};
